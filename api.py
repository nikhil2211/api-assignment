from flask import Flask, request, jsonify, json, flash
import MySQLdb.cursors
import flask
import MySQLdb.cursors


from flask_mysqldb import MySQL


app = Flask(__name__)

app.config['MYSQL_HOST'] = "localhost"
app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = ""
app.config['MYSQL_DB'] = "users_database"

mysql = MySQL(app)


@app.route('/')
def home():
    return "Hello, World!"



    #   USER REGISTRATION
@app.route('/user', methods=['POST'])
def add_user():

    # get data from request
    _name = request.json['name']
    _phone = request.json['phone']
    _email = request.json['email']
    _password = request.json['password']

    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM details WHERE email LIKE %s', [_email])
    data = cur.fetchone()
    if data:
        return "user with this email already present"

    else:
        cur.execute("INSERT INTO details (name,phone,email,password) VALUES (%s,%s,%s,%s)",(_name, _phone, _email, _password))

    mysql.connection.commit()
    cur.close()
    return flask.jsonify(message="User successfully Registered!")
    



        #  USER LOGIN
@app.route('/login',methods=['POST'])
def login():
    # if request.method == "POST":
    _email = request.json.get('email')
    _password = request.json.get('password')
    # cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

    cur = mysql.connection.cursor()

    cur.execute("SELECT * FROM details WHERE email LIKE %s AND password LIKE %s",  [_email, _password])
    data = cur.fetchone()
    if data:
        return "LOGIN SUCCESSFUL!"
        # return flask.jsonify(data)
    else:
        return "please login with valid credentials"


    #    view all the users
@app.route('/view_users')
def all_user():
    cur = mysql.connection.cursor()
    cur.execute("SELECT name,email,phone FROM details")
    data = cur.fetchall()

    cursor.close()
    return flask.jsonify(data)
    




        # Update the password
@app.route('/update', methods=['POST'])
def update_password():
    _email = request.json['email']
    _password = request.json['password']

    cur = mysql.connection.cursor()
    cur.execute("UPDATE details SET password = %s    WHERE email  =  %s", [_password, _email])
    mysql.connection.commit()
    cur.close()
    return flask.jsonify(message="updated successfully")
    


if __name__ == '__main__':
    app.run(debug=True)