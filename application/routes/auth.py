import email
from application.utilities.flask import APIResponse
from flask import Blueprint
from flask import Response, request, jsonify, make_response, render_template
from application.configs.jwt import JWTConfig
from application.models.user import User
from application import db
import jwt
from werkzeug.security import generate_password_hash,check_password_hash

auth_bp = Blueprint("auth", __name__)

# test route 1


@auth_bp.route('/signup', methods=['POST'])
def signup():

	# gets name, email and password
	email = request.json.get('email')
	password = generate_password_hash(request.json.get('password')) 
	phone_number = request.json.get('phone_number')
	name = request.json.get('name')

	# checking for existing user
	user = User.query\
		.filter_by(email=email)\
		.first()
	if not user:
		# database ORM object
		user = User(
			name=name,
			email=email,
			password=password,
            phone_number=phone_number
		)
		# insert user
		db.session.add(user)
		db.session.commit()

		return make_response('Successfully registered.', 201)
	else:
		# returns 202 if user already exists
		return make_response('User already exists. Please Log in.', 202)


@auth_bp.route('/login', methods =['POST'])
def login():
	# creates dictionary of form data
	auth = request.json

	if not auth or not auth.get('email') or not auth.get('password'):
		# returns 401 if any email or / and password is missing
		return make_response(
			'Could not verify',
			401,
			{'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
		)

	user = User.query\
		.filter_by(email = request.json.get('email'))\
		.first()

	if not user:
		# returns 401 if user does not exist
            print("hello")
            return make_response(
                'Could not verify',
                401,
                {'WWW-Authenticate' : 'Basic realm ="User does not exist !!"'}
            )

	if user.password== auth.get('password'):
		# generates the JWT Token
		token = jwt.encode({
			'email': user.email,
			# 'exp' : datetime.utcnow() + timedelta(minutes = 30)
		}, "symbtech",algorithm="HS256"
    ).decode('utf-8')
	

	return make_response(jsonify({'token' : token}), 201)
	# returns 403 if password is wrong
	return make_response(
		'Could not verify',
		403,
		{'WWW-Authenticate' : 'Basic realm ="Wrong Password !!"'}
	)
