from application.utilities.flask import APIResponse
from flask import Blueprint
from flask import Response
from application.configs.jwt import JWTConfig

test_bp = Blueprint("test", __name__)

# test route 1
@test_bp.route("/", methods=["GET", "POST"])
def test():
    return Response("<h1>Project setup successfull</h1>")


# test route3
@test_bp.route("/test", methods=["GET", "POST"])
def test_route():
    return APIResponse("Project Setup successfull")
    

