import os

class DbProductionConfig:
    DB_URI = "mysql://root@localhost/nikhil"
    DB_CONNECTION_STRING = os.environ.get(
        "DB_CONNECTION_STRING", os.environ.get("SQLALCHEMY_DATABASE_URI", DB_URI)
    )


class DbQAConfig:
    DB_URI = "mysql://root@localhost/nikhil"
    DB_CONNECTION_STRING = os.environ.get(
        "DB_CONNECTION_STRING", os.environ.get("SQLALCHEMY_DATABASE_URI", DB_URI)
    )


class DbDevelopmentConfig:
    DB_URI = "mysql://root@localhost/nikhil"

    DB_CONNECTION_STRING = os.environ.get(
        "DB_CONNECTION_STRING", os.environ.get("SQLALCHEMY_DATABASE_URI", DB_URI)
    )
